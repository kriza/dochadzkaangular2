import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `
        <h1>{{title}}</h1>
        <div id="content">
            {{content}}
        </div>
        <div id="footer">
            {{footer}}
        </div>
    `,
    styles: [ "h1 { color: blue; }",
        "#footer { font-size: 0.7em; color: #999; }",
        "#content { background-color: #eee; }"
    ]
})
export class AppComponent {
    
    title:string;
    content:string;
    footer:string;

    constructor() {
        this.title = "Unicorn dochadzka";
        this.footer = "All rights reserved";

        this.content = "";
    }

    ngOnInit() {
        this.title = "NEW Title";
    }
}
