import { Component } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'my-app',
    templateUrl: 'app7/app.component.html',
    styleUrls: ['app7/css/style.css']
})
export class AppComponent {
    
    title:string;
    footer:string;

    nameList:Array<Person>;

    constructor() {
        this.title = "Unicorn dochadzka";
        this.footer = "All rights reserved";

        this.nameList = new Array<Person>()
    }

    userUpdated(event:any):void {
        this.nameList.push(event);
        console.log(this.nameList);
    }
}
