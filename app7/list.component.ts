import { Component, Input } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'user-list',
    template: `
        <p *ngFor="let user of userList">
        {{user.firstName}} {{user.lastName}} {{user.isic ? user.isic : "-"}}
        </p>
    `
})
export class ListComponent {
    
    @Input('userList')
    userList:Array<Person>;

    constructor() {}
}
