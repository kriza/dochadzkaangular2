import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from './model/person';
import { UserService } from './service/user.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
    selector: 'user-form',
    templateUrl: 'app10/form.component.html',
})
export class FormComponent {
    
    newPerson:Person;
    index: number;
    private routeSub: Subscription;

    constructor(private userService: UserService,
                private route: ActivatedRoute,
                private router: Router) {
        this.newPerson = new Person("", "");

        this.routeSub = this.route.params.subscribe((params:Array<String>) => {
            if (params['index'] != "new") {
                this.index = params['index'];
                this.userService.list().subscribe((users:Array<Person>) => {
                    this.newPerson = users[params['index']];
                });
            }
        });
    }

    saveName():void {
        if (this.index) {
            this.userService.edit(this.newPerson, this.index).subscribe((users:Array<Person>) => {
                this.router.navigate(['list']);
            });
        } else {
            this.userService.add(this.newPerson).subscribe((users:Array<Person>) => {
                this.router.navigate(['list']);
            });
        }

        this.newPerson = new Person("", "");
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
