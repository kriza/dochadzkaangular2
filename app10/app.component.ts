import { Component } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'my-app',
    templateUrl: 'app10/app.component.html',
    styleUrls: ['app10/css/style.css']
})
export class AppComponent {
    
    title:string;
    footer:string;

    constructor() {
        this.title = "Unicorn dochadzka";
        this.footer = "All rights reserved";

    }

}
