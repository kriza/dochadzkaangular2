import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Person } from './model/person';
import { UserService } from './service/user.service';

@Component({
    selector: 'user-form',
    templateUrl: 'app9/form.component.html',
})
export class FormComponent {
    
    newPerson:Person;

    constructor(private userService: UserService) {
        this.newPerson = new Person("", "");
    }

    saveName():void {
        this.userService.add(this.newPerson).subscribe(users => {
            console.log(users);
        });

        this.newPerson = new Person("", "");
    }
}
