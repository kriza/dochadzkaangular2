import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';

import { AppComponent }  from './app.component';
import { ListComponent } from './list.component';
import { FormComponent } from './form.component';

import { UserService } from './service/user.service';

@NgModule({
  imports:      [ 
    BrowserModule,
    FormsModule 
  ],
  declarations: [ 
    AppComponent,
    ListComponent,
    FormComponent
  ],
  bootstrap:    [ AppComponent ],
  providers: [
    UserService
  ]
})
export class AppModule { }
