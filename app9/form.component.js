"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
const person_1 = require("./model/person");
const user_service_1 = require("./service/user.service");
let FormComponent = class FormComponent {
    constructor(userService) {
        this.userService = userService;
        this.newPerson = new person_1.Person("", "");
    }
    saveName() {
        this.userService.add(this.newPerson).subscribe(users => {
            console.log(users);
        });
        this.newPerson = new person_1.Person("", "");
    }
};
FormComponent = __decorate([
    core_1.Component({
        selector: 'user-form',
        templateUrl: 'app9/form.component.html',
    }),
    __metadata("design:paramtypes", [user_service_1.UserService])
], FormComponent);
exports.FormComponent = FormComponent;
//# sourceMappingURL=form.component.js.map