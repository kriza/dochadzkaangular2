import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { Person } from '../model/person';

export class UserService {

    private users:Array<Person>;
    
    constructor() {
        this.users = [
            new Person("Adam","Krt"),
            new Person("Ondras","Machula", "56316846519"),
            new Person("Jozef","Mak", "1234567890")
            ];
    }

    public list() {
        return new Observable<Array<Person>>((subscriber: Subscriber<Array<Person>>) => {
            subscriber.next(this.users);
        });
    }

    public add(person:Person) {
        return new Observable<Array<Person>>((subscriber: Subscriber<Array<Person>>) => {
            this.users.push(person);
            subscriber.next(this.users);
        });
    }

}