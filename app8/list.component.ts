import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'user-list',
    template: `
        <p *ngFor="let user of userList; let i = index" [attr.data-index]="i">
        {{user.firstName}} {{user.lastName}} {{user.isic ? user.isic : "-"}} 
        <button (click)="edit(i)">uprav</button>
        </p>
    `
})
export class ListComponent {
    
    @Input('userList')
    userList:Array<Person>;

    @Output('editUser')
    editUser = new EventEmitter();

    constructor() {}

    edit(index:number):void {
        this.editUser.emit(index);
    }
}
