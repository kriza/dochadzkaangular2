import { Component, Output, Input, EventEmitter } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'user-form',
    templateUrl: 'app8/form.component.html',
})
export class FormComponent {
    
    @Output('updatePerson')
    userUpdated = new EventEmitter();

    @Input('editPerson')
    newPerson:Person;

    constructor() {
    }

    saveName():void {
        this.userUpdated.emit(this.newPerson);

        this.newPerson = new Person("", "");
    }
}
