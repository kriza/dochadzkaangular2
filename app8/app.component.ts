import { Component } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'my-app',
    templateUrl: 'app8/app.component.html',
    styleUrls: ['app8/css/style.css']
})
export class AppComponent {
    
    title:string;
    footer:string;

    nameList:Array<Person>;
    editedUser:Person;
    editedIndex:number;

    constructor() {
        this.title = "Unicorn dochadzka";
        this.footer = "All rights reserved";

        this.nameList = new Array<Person>();
        this.editedUser = new Person("", "");
        this.editedIndex = -1;
    }

    userUpdated(event:Person):void {
        if (this.editedIndex > -1) {
            this.nameList[this.editedIndex] = event;
        } else {
            this.nameList.push(event);
        }

        this.editedUser = new Person("", "");
        this.editedIndex = -1;
        console.log(this.nameList);
    }

    editUser(index:number):void {
        this.editedIndex = index;
        this.editedUser = this.nameList[index];
    }
}
