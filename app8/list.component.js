"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const core_1 = require("@angular/core");
let ListComponent = class ListComponent {
    constructor() {
        this.editUser = new core_1.EventEmitter();
    }
    edit(index) {
        this.editUser.emit(index);
    }
};
__decorate([
    core_1.Input('userList'),
    __metadata("design:type", Array)
], ListComponent.prototype, "userList", void 0);
__decorate([
    core_1.Output('editUser'),
    __metadata("design:type", Object)
], ListComponent.prototype, "editUser", void 0);
ListComponent = __decorate([
    core_1.Component({
        selector: 'user-list',
        template: `
        <p *ngFor="let user of userList; let i = index" [attr.data-index]="i">
        {{user.firstName}} {{user.lastName}} {{user.isic ? user.isic : "-"}} 
        <button (click)="edit(i)">uprav</button>
        </p>
    `
    }),
    __metadata("design:paramtypes", [])
], ListComponent);
exports.ListComponent = ListComponent;
//# sourceMappingURL=list.component.js.map