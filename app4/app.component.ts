import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    templateUrl: 'app4/app.component.html',
    styleUrls: ['app4/css/style.css']
})
export class AppComponent {
    
    title:string;
    footer:string;

    nameList:Array<string>;

    constructor() {
        this.title = "Unicorn dochadzka";
        this.footer = "All rights reserved";

        this.nameList = new Array<string>();
    }

    ulozMeno(meno:string):void {
        this.nameList.push(meno);
        console.log(this.nameList);
    }
}
