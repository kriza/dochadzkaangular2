import { Component } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'my-app',
    templateUrl: 'app6/app.component.html',
    styleUrls: ['app6/css/style.css']
})
export class AppComponent {
    
    title:string;
    footer:string;

    nameList:Array<Person>;
    newPerson:Person;

    constructor() {
        this.title = "Unicorn dochadzka";
        this.footer = "All rights reserved";

        this.nameList = new Array<Person>();
        this.newPerson = new Person("","");
    }

    saveName():void {
        this.nameList.push(this.newPerson);
        this.newPerson = new Person("","");
        console.log(this.nameList);
    }
}
