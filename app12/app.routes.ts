import { ModuleWithProviders }  from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent }  from './app.component';
import { ListComponent } from './list.component';
import { FormComponent } from './form.component';

const routes: Routes = [
  { path: 'user/:index', component: FormComponent },
  { path: 'list', component: ListComponent },
  { path: 'user/new', component: FormComponent },
  { path: '', redirectTo: 'list', pathMatch: 'full' }
];


export const routing: ModuleWithProviders = RouterModule.forRoot(routes);