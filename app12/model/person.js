"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Person {
    constructor(firstName, lastName, isic) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isic = isic ? isic : "";
    }
}
exports.Person = Person;
//# sourceMappingURL=person.js.map