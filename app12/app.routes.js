"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const router_1 = require("@angular/router");
const list_component_1 = require("./list.component");
const form_component_1 = require("./form.component");
const routes = [
    { path: 'user/:index', component: form_component_1.FormComponent },
    { path: 'list', component: list_component_1.ListComponent },
    { path: 'user/new', component: form_component_1.FormComponent },
    { path: '', redirectTo: 'list', pathMatch: 'full' }
];
exports.routing = router_1.RouterModule.forRoot(routes);
//# sourceMappingURL=app.routes.js.map