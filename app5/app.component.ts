import { Component } from '@angular/core';
import { Person } from './model/person';

@Component({
    selector: 'my-app',
    templateUrl: 'app5/app.component.html',
    styleUrls: ['app5/css/style.css']
})
export class AppComponent {
    
    title:string;
    footer:string;

    nameList:Array<Person>;

    constructor() {
        this.title = "Unicorn dochadzka";
        this.footer = "All rights reserved";

        this.nameList = new Array<Person>()
    }

    saveName(name:string, surname:string):void {
        this.nameList.push(new Person(name, surname));
        console.log(this.nameList);
    }
}
