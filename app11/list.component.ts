import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Person } from './model/person';
import { UserService } from './service/user.service';

@Component({
    selector: 'user-list',
    template: `
        <md-list>
          <md-list-item *ngFor="let user of userList; let i = index" [attr.data-index]="i">
            <h3 md-line> {{user.firstName}} {{user.lastName}}</h3>
            <p md-line> {{user.isic ? user.isic : "-"}} </p>
            <button (click)="edit(i)" md-mini-fab><md-icon>edit</md-icon></button>
          </md-list-item>
        </md-list>
        
    `
})
export class ListComponent {
    
    userList:Array<Person>;

    constructor(private userService: UserService,
                private router: Router) {}

    ngOnInit() {
        this.userService.list().subscribe((users:Array<Person>) => {
            this.userList = users;
        });
    }

    edit(index:number):void {
        this.router.navigate(['user/'+index]);
    }
}
